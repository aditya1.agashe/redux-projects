import React from 'react';
import ReactDOM from 'react-dom';
import CommentDetail from './CommentDetail';
import Faker from 'faker';
import ApprovalCard from './ApprovalCard';

const App = () => {
    return (
        <div className="ui container comments" >
            <ApprovalCard/>
            <CommentDetail
                author={Faker.name.firstName()}
                timeAgo="Today at 4:45 PM"
                imgSrc={Faker.image.avatar()}
                commentTxt={Faker.lorem.sentence()} />
            <CommentDetail
                author={Faker.name.firstName()}
                timeAgo="Today at 2:00 PM"
                imgSrc={Faker.image.avatar()}
                commentTxt={Faker.lorem.sentence()} />
            <CommentDetail
                author={Faker.name.firstName()}
                timeAgo="Yesterday at 4:45 PM"
                imgSrc={Faker.image.avatar()}
                commentTxt={Faker.lorem.sentence()} />
        </div>
    );
};

ReactDOM.render(<App />, document.querySelector('#root'));